﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Archivos
{
    public partial class FormPadre : Form
    {
        public FormPadre()
        {
            InitializeComponent();

            LblRuta.Text = string.Empty;
        }

        #region Funciones de Ayuda
        protected string ColocarTextoEnUnaLinea()
        {
            return $"{TxtCuenta.Text},{TxtNombre.Text},{TxtApellido.Text},{TxtSaldo.Text}";
        }

        protected void LimpiarTextBox()
        {
            TxtApellido.Text = TxtNombre.Text = TxtCuenta.Text = TxtSaldo.Text = string.Empty;
        }

        protected void MensajeDeError(string mensaje)
        {
            MessageBox.Show(mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        #endregion

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void streamWriterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frmWriter = new StreamWriterForm();
            frmWriter.Show();
        }

        private void streamReaderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frmReader = new StreamReaderForm();
            frmReader.ShowDialog();
        }
    }
}
