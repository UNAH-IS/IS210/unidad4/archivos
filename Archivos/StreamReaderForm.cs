﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Archivos
{
    public partial class StreamReaderForm : Archivos.FormPadre
    {
        StreamReader reader;

        public StreamReaderForm()
        {
            InitializeComponent();

            BtnSiguiente.Enabled = false;
        }

        private void BtnAbrir_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                LblRuta.Text = openFileDialog1.FileName;
                reader = new StreamReader(LblRuta.Text);
                BtnAbrir.Enabled = false;
                BtnSiguiente.Enabled = true;
            }
        }

        private void BtnSiguiente_Click(object sender, EventArgs e)
        {
            if (reader == null)
            {
                return;
            }

            try
            {
                string linea = reader.ReadLine();

                if (linea != null)
                {
                    string[] campos = linea.Split(',');

                    TxtCuenta.Text = campos[0];
                    TxtNombre.Text = campos[1];
                    TxtApellido.Text = campos[2];
                    TxtSaldo.Text = campos[3];
                } else
                {
                    reader.Close();
                    BtnAbrir.Enabled = true;
                    LimpiarTextBox();

                    MessageBox.Show("No hay mas resultados", 
                        "Aviso", 
                        MessageBoxButtons.OK, 
                        MessageBoxIcon.Information);
                }

            } catch(IOException)
            {
                MensajeDeError("Error al manipular el archivo");
            }
        }
    }
}
