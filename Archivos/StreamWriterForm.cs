﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Archivos
{
    public partial class StreamWriterForm : Archivos.FormPadre
    {
        StreamWriter writer;

        public StreamWriterForm()
        {
            InitializeComponent();

            BtnGuardar.Enabled = false;
        }
        private void BtnAbrir_Click(object sender, EventArgs e)
        {
            DialogResult result;

            //saveFileDialog1.CheckFileExists = false;

            // Aqui se guarda la eleccion del usuario (OK o CANCEL)
            result = saveFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                LblRuta.Text = saveFileDialog1.FileName;
                BtnGuardar.Enabled = true;
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                writer = new StreamWriter(LblRuta.Text, true);
                writer.WriteLine(ColocarTextoEnUnaLinea());
            }
            catch (IOException)
            {
                MensajeDeError("Problemas con el archivo");
            }
            finally
            {
                // No importa lo que pase limpiara los textbox
                writer?.Close();
                LimpiarTextBox();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ruta = @"C:\pool\test.txt";
            string[] contenido = { "hola", "mundo" };

            File.WriteAllLines(ruta, contenido);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string ruta = @"C:\pool\test.txt";
            string[] contenido = File.ReadAllLines(ruta, Encoding.UTF8);

            listBox1.Items.AddRange(contenido);
        }
    }
}
